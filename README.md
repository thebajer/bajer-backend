# The BAJer backend

## Overview

This repository provides the backend server for the BAJer.

It is using [connexion](https://github.com/zalando/connexion).
The Swagger / OpenAPI file defining the API can be found at [swagger/swagger.yaml](swagger/swagger.yaml).

## Installation

Install dependencies:
``` bash
pip install -r requirements.txt
```

Create config file at `~/.bajer/bajer-backend.yaml` with the following content:
``` yaml
database: postgres://<string to database>
```
The user directory needs to be of the user under which the server is being run.

To start it run:
``` bash
gunicorn app:app -b 0.0.0.0:8000 --log-file logs/gunicorn.log --log-level WARN --reload --timeout 300
```
Adapt the `gunicorn` parameters as needed.


### FreeBSD Service

Use the included [rc file](freebsd-rc-file) as a template and adapt accordingly.
It assumes a user `bajer` and the `bajer-backend` located in `/home/bajer/bajer-backend`.
