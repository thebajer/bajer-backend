#!/usr/bin/env python3

import connexion
from connexion import NoContent
import logging
import orm
import argparse
from pathlib import Path
import yaml

home = str(Path.home())

config = yaml.safe_load(open(home + "/.bajer/bajer-backend.yaml"))

db_session = None

SQLALCHEMY_DATABASE_URI = config["database"]


logging.basicConfig(level=logging.INFO)
db_session = orm.init_db(SQLALCHEMY_DATABASE_URI)

app = connexion.App(__name__, specification_dir="./swagger/")
app.add_api(
    "swagger.yaml",
    arguments={
        "title": "API to interface with the BAJer (Bibliometrics for Astronomical Journals)"
    },
)


application = app.app


@application.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == "__main__":
    app.run(port=8000)
