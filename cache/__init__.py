import hashlib

custom_cache = {}

# Created because functools.lru_cache has problems with the lists passed to
# controller functions as lists are not hashable

def add_to_cache(*args, result):
    """Create hash of *args and store result in cache"""
    data = ""
    for arg in args:
        data = data + str(arg)

    h = hashlib.sha256(data.encode()).hexdigest()
    custom_cache[h] = result

def get_from_cache(*args):
    """Create hash of *args and checks if it is stored in cache"""
    data = ""
    for arg in args:
        data = data + str(arg)

    h = hashlib.sha256(data.encode()).hexdigest()
    try:
        return custom_cache[h]
    except KeyError as e:
        return False
