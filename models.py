# coding: utf-8
from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, SmallInteger, String, Table, Text
from sqlalchemy.orm import relationship
from sqlalchemy.schema import FetchedValue
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Author(Base):
    __tablename__ = 'authors'

    author = Column(String(255), primary_key=True)

    papers = relationship('Paper', secondary=None, backref='authors')


t_authors_papers = Table(
    'authors_papers', metadata,
    Column('author_authors', ForeignKey('authors.author', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False),
    Column('bibcode_papers', ForeignKey('papers.bibcode', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False)
)


class CitingPaper(Base):
    __tablename__ = 'citing_papers'

    bibcode = Column(String(19), primary_key=True)
    year = Column(SmallInteger, nullable=False)
    first_fetched = Column(Date, nullable=False)

    papers = relationship('Paper', secondary=None, backref='citing_papers')


class FetchDate(Base):
    __tablename__ = 'fetch_dates'

    date = Column(Date, primary_key=True)
    released = Column(Boolean, nullable=False, server_default=FetchedValue())


class History(Base):
    __tablename__ = 'histories'

    citation_count = Column(Integer, nullable=False)
    paper = Column(ForeignKey('papers.bibcode', ondelete='CASCADE', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False)
    date = Column(ForeignKey('fetch_dates.date', ondelete='CASCADE', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False)

    fetch_date = relationship('FetchDate', primaryjoin='History.date == FetchDate.date', backref='histories')
    paper1 = relationship('Paper', primaryjoin='History.paper == Paper.bibcode', backref='histories')


class Journal(Base):
    __tablename__ = 'journals'

    bibstem = Column(String(10), primary_key=True)
    name = Column(String(255))


class Keyword(Base):
    __tablename__ = 'keywords'

    keyword = Column(String(600), primary_key=True)


t_keywords_papers = Table(
    'keywords_papers', metadata,
    Column('keyword_keywords', ForeignKey('keywords.keyword', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False),
    Column('bibcode_papers', ForeignKey('papers.bibcode', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False)
)


class Paper(Base):
    __tablename__ = 'papers'

    bibcode = Column(String(19), primary_key=True)
    title = Column(Text, nullable=False)
    abstract = Column(Text)
    first_author = Column(ForeignKey('authors.author', match='FULL'))
    pubdate = Column(Date, nullable=False)
    journal = Column(ForeignKey('journals.bibstem', ondelete='SET NULL', onupdate='CASCADE', match='FULL'))

    author = relationship('Author', primaryjoin='Paper.first_author == Author.author', backref='papers')
    journal1 = relationship('Journal', primaryjoin='Paper.journal == Journal.bibstem', backref='papers')
    keywords = relationship('Keyword', secondary=None, backref='papers')


t_papers_citing_papers = Table(
    'papers_citing_papers', metadata,
    Column('bibcode_papers', ForeignKey('papers.bibcode', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False),
    Column('bibcode_citing_papers', ForeignKey('citing_papers.bibcode', ondelete='RESTRICT', onupdate='CASCADE', match='FULL'), primary_key=True, nullable=False)
)
