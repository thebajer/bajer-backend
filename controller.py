from app import db_session
from datetime import datetime
import cache as c


def run_std_query(
    fetchDate, minYear, maxYear, bibstem, db_session, db_query, **kwargs
):
    fetch = datetime.strptime(fetchDate, "%Y-%m-%d")
    startYear = datetime(int(minYear), 1, 1)
    endYear = datetime(
        int(maxYear) + 1, 1, 1
    )  # add one year to include the selected

    params = {
        "fetchDate": fetch,
        "startYear": startYear,
        "endYear": endYear,
        "journals": tuple(bibstem),
    }

    for key, value in kwargs.items():
        params[key] = value

    res = db_session.execute(db_query, params=params)

    return res


def run_2d_query(
    fetchDate, minYear, maxYear, bibstem, db_session, db_query
) -> str:
    # check if we got something in our cache
    cached = c.get_from_cache(fetchDate, minYear, maxYear, bibstem, db_query)
    if cached is not False:
        return cached
    res = run_std_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )
    bibstems = []
    years = []
    data = []

    for row in res.fetchall():
        bibstems.append(row[0])
        years.append(row[1])
        data.append(row[2])

    results = {"bibstem": bibstems, "xAxis": years, "yAxis": data}

    # cache results
    c.add_to_cache(
        fetchDate,
        minYear,
        maxYear,
        bibstem,
        db_query,
        result=results,
    )

    return results


def bibstem_get() -> str:
    res = db_session.execute(
        """SELECT bibstem, name
        FROM journals
        ORDER by bibstem"""
    )
    results = {}
    for row in res.fetchall():
        results[row[0]] = row[1]
    return {"bibstem": results}


def fetch_dates_get() -> str:
    res = db_session.execute(
        "SELECT date FROM fetch_dates WHERE released=True ORDER BY date DESC"
    )
    results = []
    for row in res.fetchall():
        results.append(row[0].strftime("%Y-%m-%d"))
    return {"date": results}


def year_range_get() -> str:
    res = db_session.execute(
        "SELECT extract('year' from min(pubdate)) as minyear, extract('year' from max(pubdate)) as maxyear FROM papers"
    )
    result = ""
    for row in res.fetchall():
        result = {"minYear": row[0], "maxYear": row[1]}
    return result


def published_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as year,
    count(histories.paper) as paper_count
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    year,
    bibstem
ORDER BY
	bibstem,
    year;"""

    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def cited_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as year,
    sum(histories.citation_count) as citation_count
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    histories.citation_count > 0
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    year,
    bibstem
ORDER BY
	bibstem,
    year;"""
    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def cites_to_pub_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as year,
    CAST(SUM(histories.citation_count) as float) / CAST(COUNT(histories.paper) as float) as cites_to_pub_ratio
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    year,
    bibstem
ORDER BY
    bibstem,
    year;"""
    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def zero_cites_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as year,
    count(histories.paper) as paper_count
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    histories.citation_count = 0
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    year,
    bibstem
ORDER BY
    bibstem,
    year;"""
    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def zero_cites_normalized_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """WITH zerocites AS (SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as years,
    count(histories.paper) as zeros
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    histories.citation_count = 0
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    years,
    bibstem
ORDER BY
    bibstem,
    years
    ), combined AS (
SELECT
	papers.journal AS bibstem,
	EXTRACT('year' FROM date_trunc('year', papers.pubdate)) AS years,
	count(histories.paper) AS published
FROM
	papers
	JOIN
		histories
	ON
		histories.paper = papers.bibcode
WHERE
	histories.date = :fetchDate
AND
	papers.pubdate >= :startYear
AND
	papers.pubdate < :endYear
AND
    papers.journal IN :journals
GROUP BY
	years,
	bibstem
ORDER BY
	bibstem,
	years
    )

SELECT
	zerocites.bibstem AS bibstem,
	zerocites.years AS years,
	cast(zerocites.zeros AS float) / cast(combined.published AS float) AS zero_cite_ratio2
FROM zerocites
JOIN
	combined
ON
	zerocites.bibstem = combined.bibstem
AND zerocites.years = combined.years;"""
    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def first_author_per_country_per_year_get(
    fetchDate, minYear, maxYear, bibstem, topx=10
) -> str:
    db_query = """WITH pre AS (
    SELECT papers.journal as journal, countries.country as country, count(histories.paper) as amount
    FROM papers
        JOIN
            countries
        ON
            countries.id = papers.first_author_country
        JOIN
            histories
        ON
            histories.paper = papers.bibcode
        WHERE
            histories.date = :fetchDate
        AND
            papers.pubdate >= :startYear
        AND
            papers.pubdate < :endYear

        AND
            papers.journal IN :journals
    GROUP BY journal, country
    ORDER BY journal, amount DESC)

    SELECT journal, country, amount
    FROM (
        SELECT *, row_number()
        OVER (
            PARTITION BY
                journal
            ORDER BY
                amount DESC
        ) AS rno
        FROM pre
    ) t1
    WHERE rno <= :topx"""
    # check if we got something in our cache
    cached = c.get_from_cache(
        fetchDate, minYear, maxYear, bibstem, db_query, topx
    )
    if cached is not False:
        return cached

    res = run_std_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query, topx=topx
    )
    bibstems = []
    countries = []
    amount = []

    for row in res.fetchall():
        bibstems.append(row[0])
        countries.append(row[1].title())
        amount.append(row[2])

    results = {"bibstem": bibstems, "country": countries, "amount": amount}

    # cache results
    c.add_to_cache(
        fetchDate,
        minYear,
        maxYear,
        bibstem,
        db_query,
        topx,
        result=results,
    )

    return results


def citations_to_global_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """
    WITH allcites as (
SELECT
    extract('year' from date_trunc('year',papers.pubdate)) as years,
    cast(sum(histories.citation_count) as float) as cites
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
WHERE
    histories.date =  :fetchDate
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
GROUP BY
    years
ORDER BY
    years
), journal_cites as (
SELECT
    papers.journal as bibstem,
    extract('year' from date_trunc('year',papers.pubdate)) as year,
    cast(allcites.cites as decimal) as global_cites,
    cast(sum(histories.citation_count) as float) as cites
FROM
    papers
    JOIN
        histories
        ON
        histories.paper = papers.bibcode
    join allcites
        on
        allcites.years = extract('year' from date_trunc('year',papers.pubdate))
WHERE
    histories.date =  :fetchDate
    AND
    papers.pubdate >= :startYear
    AND
    papers.pubdate < :endYear
    AND
    papers.journal IN :journals
GROUP BY
    year,
    bibstem,
    global_cites
ORDER BY
    bibstem,
    global_cites,
    year)

SELECT
	bibstem,
	year,
	cites / NULLIF(global_cites, 0) as ratio
FROM journal_cites
ORDER BY bibstem, year;
    """
    return run_2d_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )


def all_papers_basic_get(fetchDate, minYear, maxYear, bibstem) -> str:
    db_query = """SELECT
	papers.bibcode,
	date_part('year', papers.pubdate) AS year,
	papers.journal,
	histories.citation_count
FROM
	papers
	JOIN histories
	ON histories.paper = papers.bibcode
WHERE
    histories.date = :fetchDate
	AND histories.citation_count > 0
    AND papers.pubdate >= :startYear
    AND papers.pubdate < :endYear
    AND papers.journal IN :journals;"""
    # check if we got something in our cache
    cached = c.get_from_cache(fetchDate, minYear, maxYear, bibstem, db_query)
    if cached is not False:
        return cached

    res = run_std_query(
        fetchDate, minYear, maxYear, bibstem, db_session, db_query
    )
    bibcodes = []
    years = []
    bibstems = []
    citation_counts = []

    for row in res.fetchall():
        bibcodes.append(row[0])
        years.append(row[1])
        bibstems.append(row[2])
        citation_counts.append(row[3])

    results = {
        "bibcode": bibcodes,
        "year": years,
        "bibstem": bibstems,
        "citation_count": citation_counts,
    }

    # cache results
    c.add_to_cache(
        fetchDate,
        minYear,
        maxYear,
        bibstem,
        db_query,
        result=results,
    )

    return results
